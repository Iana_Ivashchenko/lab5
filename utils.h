#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

void print_int(int a)
{
    printf("%d ", a);
}

void print_int_newline(int a)
{
    printf("%d\n", a);
}

int sqr(int a)
{
    return a * a;
}

int cube(int a)
{
    return a * a * a;
}

int sum(int a, int b)
{
    return a + b;
}

int min(int a, int b)
{
    return a > b ? b : a;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

int module(int a)
{
    return a > 0 ? a : -1 * a;
}

int pow_two(int a)
{
    return a * 2;
}


#endif // UTILS_H

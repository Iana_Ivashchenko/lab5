#include "listfunctions.h"
#include "utils.h"
#include <limits.h>

void print_menu()
{
    printf("-----------------------------\n");
    printf("1.Add an element to the front\n");
    printf("2.Add an element to the back\n");
    printf("3.Search element\n");
    printf("4.Sum of all elements\n");
    printf("5.High-order functions demo\n");
    printf("0.Exit\n");
    printf("-----------------------------\n");
}

void demo(node* list)
{
    printf("Foreach test 1:\n");
    foreach(list, print_int);
    printf("\n");

    printf("Foreach test 2:\n");
    foreach(list, print_int_newline);

    printf("Map test 1 (sqr):\n");
    node* new_list = map(list, sqr);
    list_print(new_list);

    printf("Map test 1 (cube):\n");
    new_list = map(list, cube);
    list_print(new_list);

    printf("Foldl test 1 (sum): %d\n", foldl(list, 0, sum));

    printf("Foldl test 2 (min): %d\n", foldl(list, INT_MAX, min));

    printf("Foldl test 3 (max): %d\n", foldl(list, INT_MIN, max));

    printf("Map_mut test (abs):\n");
    new_list = list;
    map_mut(new_list, module);
    list_print(new_list);

    printf("Iterate test (pow 2):\n");
    new_list = iterate(1,10,pow_two);
    list_print(new_list);

    node* array = NULL;
    printf("Save test\n");
    save(list, "textfile");

    printf("Load test\n");
    load(&array, "textfile");
    list_print(array);

    array = NULL;
    printf("Serialize test\n");
    serialize(list, "binfile");

    printf("Deserialize test\n");
    deserialize(&array, "binfile");
    list_print(array);
}



int main()
{
    int command = 0;
    int value;

    node* list = NULL;
    int tmp = 0;
    int sum = 0;
    print_menu();
    while(1)
    {
        scanf("%d",&command);
        if(command == 0)
            break;
        switch(command)
        {
        case 1:
            printf("Enter a number to add:\n");
            if(scanf("%d",&value) <=0)
            {
                fseek(stdin,0,SEEK_END);
                printf("Wrong input!\n");
                break;
            }
            list = list_add_front(list,value);
            list_print(list);
            break;
        case 2:
            printf("Enter a number to add:\n");
            if(scanf("%d",&value) <=0)
            {
                fseek(stdin,0,SEEK_END);
                printf("Wrong input!\n");
                break;
            }
            list = list_add_back(list,value);
            list_print(list);
            break;
        case 3:
            printf("Enter a number to search:\n");
            if(scanf("%d",&value) <=0)
            {
                fseek(stdin,0,SEEK_END);
                printf("Wrong input!\n");
                break;
            }
            tmp = list_get(list,value);
            if(tmp != 0)
            {
                printf("Element with index %d: %d.\n",value, tmp);
            }
            else
            {
                printf("No such element!\n");
            }
            break;
        case 4:
            sum = list_sum(list);
            printf("Sum of all elements: %d\n", sum);
            break;
        case 5:
            demo(list);
            break;
        default:
            printf("No such command!\n");
            break;
        }
        print_menu();
    }
    list_free(list);
    return 0;
}
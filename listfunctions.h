#ifndef LISTFUNCTIONS_H
#define LISTFUNCTIONS_H
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>


typedef struct node
{
    int value;
    struct node* next;
} node;

node* list_create(int value,node* next);
node* list_add_front(node* list,int value);
node* list_add_back(node* list, int value);
int list_length(node *list);
void list_free(node *list);
node* list_at(node* list, int index);
int list_get(node* list,int index);
int list_sum(node *n);
void list_print(node* list);
void list_set(node* list, int index, int value);

void foreach(node* list, void (*func) (int) );
node* map(node* list, int (*func) (int) );
void map_mut(node* list, int (*func) (int) );
int foldl(node* list, int acc, int (*func) (int, int) );
node* iterate(int s, int n, int (*func) (int) );
bool save(node* list, const char* filename);
bool load(node** list, const char* filename);
bool serialize(node* list, const char* filename);
bool deserialize(node** list, const char* filename);

#endif // LISTFUNCTIONS_H

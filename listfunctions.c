#include "listfunctions.h"

node* list_create(int value,node* next)
{
    node* new_node = (node*)malloc(sizeof(node));
    new_node->value = value;
    new_node->next = next;
    return new_node;
}

node* list_add_front(node* list,int value)
{
    node* new_node = list_create(value,list);
    list = new_node;
    return list;
}

node* list_add_back(node* list, int value)
{
    if(list == NULL) return list_create(value,list);
    node *current = list;
    while(current->next != NULL)
    {
        current = current->next;
    }
    node* new_node =  list_create(value,NULL);
    current->next = new_node;
    return list;
}

int list_length(node *list)
{
    node *current = list;
    int count = 0;
    while(current != NULL)
    {
        current = current->next;
        count++;
    }
    return count;
}


void list_free(node *list)
{
    node *current, *tmp;
    if(list != NULL)
    {
        current = list->next;
        list->next = NULL;
        while(current != NULL)
        {
            tmp = current->next;
            free(current);
            current = tmp;
        }
    }
}

node* list_at(node* list, int index)
{
    if (list_length(list) < index || index < 0) return NULL;
    node* current = list;
    int i = 0;
    while(i < index)
    {
        current = current->next;
        i++;
    }
    return current;
}

int list_get(node* list,int index)
{
    if (list_length(list) < index || index < 0) return 0;
    node *res = list_at(list, index);
    if(res == NULL) return 0;
    return res->value;
}

int list_sum(node *n)
{
    int sum = 0;
    while (n != NULL)
    {
        sum += n->value;
        n = n->next;
    }
    return sum;
}

void list_print(node* list)
{
    if (list == NULL)
    {
        printf("List is empty!\n");
    }
    int i = 0;
    while (list != NULL)
    {
        printf("Element %d: %d \n", i, list->value);
        list = list->next;
        i++;
    }
}

void list_set(node* list, int index, int value)
{
    node* val = list_at(list, index);
    if(val != NULL) val->value = value;
}

void foreach(node* list, void (*func) (int) )
{
    int len = list_length(list);
    int i;
    for (i = 0; i < len; i++)
    {
        func(list_get(list, i));
    }
}

node* map(node* list, int (*func) (int) )
{
    int len = list_length(list);
    int i;
    node* new_node = NULL;
    for(i = 0; i < len; i++)
    {
        int value = func(list_get(list, i));
        new_node = list_add_back(new_node, value);
    }
    return new_node;
}

void map_mut(node* list, int (*func) (int) )
{
    int len = list_length(list);
    int i;
    for(i = 0; i < len; i++)
    {
        int value = func(list_get(list, i));
        list_set(list, i, value);
    }
}

int foldl(node* list, int acc, int (*func) (int, int) )
{
    int len = list_length(list);
    int i;
    for(i = 0; i <= len; i++)
    {
        acc = func(list_get(list, i), acc);
    }
    return acc;
}

node* iterate(int s, int n, int (*func) (int) )
{
    node* list = NULL;
    int i;
    for(i = 0; i <= n; i++)
    {
        list = list_add_back(list, s);
        s = func(s);
    }
    return list;
}

bool save(node* list, const char* filename)
{
    FILE * file = fopen(filename, "w");
    if (file == NULL) return false;
    while (list)
    {
        if (fprintf(file, "%d ", list_get(list, 0)) <= 0 ||  ferror(file))
        {
            fclose(file);
            return false;
        }
        list = list_at(list, 1);
    }
    fclose(file);
    return true;
}

bool load(node** list, const char* filename)
{
    FILE * file = fopen(filename, "r");
    int data;
    if (file == NULL) return false;
    while (fscanf(file, "%d", &data) == 1)
    {
        *list = list_add_back(*list, data);
        if (ferror(file))
        {
            fclose(file);
            return false;
        }
    }
    fclose(file);
    return true;
}


bool serialize(node* list, const char* filename)
{
    FILE * file = fopen(filename, "wb");
    if (file == NULL) return false;
    while (list)
    {
        if (fprintf(file, "%d ", list_get(list, 0)) <= 0 ||  ferror(file))
        {
            fclose(file);
            return false;
        }
        list = list_at(list, 1);
    }
    fclose(file);
    return true;
}

bool deserialize(node** list, const char* filename)
{
    FILE * file = fopen(filename, "rb");
    int data;
    if (file == NULL) return false;
    while (fscanf(file, "%d", &data) == 1)
    {
        *list = list_add_back(*list, data);
        if (ferror(file))
        {
            fclose(file);
            return false;
        }
    }
    fclose(file);
    return true;
}

